<?php

class Database
{
    private static $instance = null;
    private  $pdo;

    private function __construct()
    {
        $host = 'localhost';
        $db = 'event_management';
        $user = 'root';
        $pass = 'password';

        try {
            $this->pdo = new PDO("mysql:host=$host;dbname=$db;charset=utf8mb4", $user, $pass);
        } catch (PDOException $e) {
            die("Connection failed: " . $e->getMessage());
        }
    }

    public static function getInstance() {
        if (self::$instance === null) {
            self::$instance = new Database();
        }
        return self::$instance;
    }

    public function getConnection() {
        return $this->pdo;
    }
}