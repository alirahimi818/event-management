<?php

class TimezoneConverter
{
    public static function convertEventDate($eventDate, $version, $changeVersion = '1.0.17+60') {

        if (version_compare($version, $changeVersion, '>='))
            return new DateTime($eventDate, new DateTimeZone('UTC'));
        else
            return new DateTime($eventDate, new DateTimeZone('Europe/Berlin'));
    }
}