<?php

require_once __DIR__ . "/Database.php";

class BookingMigration
{
    private $table_name = "bookings";
    private $pdo;

    public function __construct()
    {
        $database = Database::getInstance();
        $this->pdo = $database->getConnection();
    }

    public function checkExistTable()
    {
        $result = $this->pdo->query("SHOW TABLES LIKE '{$this->table_name}'");
        return $result->rowCount() > 0;
    }

    public function create()
    {
        if (!$this->checkExistTable()) {
            $sql = "CREATE TABLE IF NOT EXISTS $this->table_name (
                participation_id INT AUTO_INCREMENT PRIMARY KEY,
                employee_name VARCHAR(255),
                employee_mail VARCHAR(255),
                event_id INT,
                event_name VARCHAR(255),
                participation_fee DECIMAL(10, 2),
                event_date DATETIME,
                version VARCHAR(20)
            )";
            $this->pdo->exec($sql);
        }
    }
}