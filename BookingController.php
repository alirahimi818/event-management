<?php

require_once __DIR__ . "/BookingMigration.php";
require_once __DIR__ . "/BookingModel.php";

class BookingController
{
    public function fetchData($request)
    {
        $bookingMigration = new BookingMigration();
        if (!$bookingMigration->checkExistTable()) {
            $bookingMigration->create();
            $this->importFromJsonFile();
        }

        return $this->filter($request);
    }

    public function filter($request)
    {
        if ($request && ($request['employee_name'] || $request['event_name'] || $request['event_date'])) {
            $bookingModel = new BookingModel();
            return $bookingModel->filter($request);
        }
        return [];
    }

    private function validate($request)
    {
        if (
            empty($request['employee_name']) ||
            empty($request['employee_mail']) ||
            empty($request['event_id']) ||
            empty($request['event_name']) ||
            empty($request['participation_fee']) ||
            empty($request['event_date']) ||
            empty($request['version'])
        ) {
            return false;
        }

        $eventDate = DateTime::createFromFormat('Y-m-d H:i:s', $request['event_date']);
        if (!$eventDate) {
            return false;
        }

        if (!is_numeric($request['participation_fee']) || $request['participation_fee'] < 0) {
            return false;
        }

        if (!filter_var($request['employee_mail'], FILTER_VALIDATE_EMAIL)) {
            return false;
        }
        return true;
    }

    public function importFromJsonFile($filePath = __DIR__ . "/export.json")
    {
        try {
            $jsonData = file_get_contents($filePath);
            $bookings = json_decode($jsonData, true);

            foreach ($bookings as $booking) {
                $this->store($booking);
            }
        } catch (Exception $e) {
            die('Import failed! ' . $e->getMessage());
        }
    }

    public function store($request)
    {
        if ($this->validate($request)) {
            $bookingModel = new BookingModel();
            $bookingModel->create($request);
        }
    }
}