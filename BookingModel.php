<?php

require_once __DIR__ . "/Database.php";

class BookingModel
{
    private $table_name = "bookings";
    private $pdo;

    public function __construct()
    {
        $database = Database::getInstance();
        $this->pdo = $database->getConnection();
    }

    public function filter($request)
    {
        $employeeName = $request['employee_name'];
        $eventName = $request['event_name'];
        $eventDate = $request['event_date'];

        $params = [];
        $sql = "SELECT * FROM $this->table_name WHERE 1";
        if (!empty($employeeName)) {
            $sql .= " AND employee_name LIKE ?";
            $params[] = "%$employeeName%";
        }

        if (!empty($eventName)) {
            $sql .= " AND event_name LIKE ?";
            $params[] = "%$eventName%";
        }

        if (!empty($eventDate)) {
            $sql .= " AND DATE(event_date) = ?";
            $params[] = $eventDate;
        }

        $stmt = $this->pdo->prepare($sql);
        $stmt->execute($params);
        return $stmt->fetchAll(PDO::FETCH_ASSOC);
    }

    public function create($request)
    {
        $stmt = $this->pdo->prepare("INSERT INTO $this->table_name (employee_name, employee_mail, event_id, event_name, participation_fee, event_date, version) VALUES (?, ?, ?, ?, ?, ?, ?)");
        $stmt->execute([$request['employee_name'], $request['employee_mail'], $request['event_id'], $request['event_name'], $request['participation_fee'], $request['event_date'], $request['version']]);
    }
}