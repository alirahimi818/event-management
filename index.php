<?php

require_once __DIR__ . "/BookingController.php";
require_once __DIR__ . "/TimezoneConverter.php";

$bookingController = new BookingController();
$rows = $bookingController->fetchData($_POST);

?>

<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Event Management</title>
</head>
<body>

<form method="post" action="">
    Employee Name: <input value="<?php echo $_POST['employee_name'] ?? '' ?>" type="text" name="employee_name">
    Event Name: <input value="<?php echo $_POST['event_name'] ?? '' ?>" type="text" name="event_name">
    Date: <input value="<?php echo $_POST['event_date'] ?? '' ?>" type="date" name="event_date">
    <input type="submit" value="Filter">
</form>

<br>
<table border="1">
    <tr>
        <th>Employee Name</th>
        <th>Employee Email</th>
        <th>Event Id</th>
        <th>Event Name</th>
        <th>Participation Fee</th>
        <th>Event Date</th>
        <th>Version</th>
    </tr>
    <?php

    if (count($rows) > 0) {
        $total_price = 0;
        foreach ($rows as $row) {
            $total_price += $row['participation_fee'];
            $date = TimezoneConverter::convertEventDate($row['event_date'],$row['version'])->format("Y-m-d H:i:s");
            echo "<tr>
                      <td>{$row['employee_name']}</td>
                      <td>{$row['employee_mail']}</td>
                      <td>{$row['event_id']}</td>
                      <td>{$row['event_name']}</td>
                      <td>{$row['participation_fee']}</td>
                      <td>$date</td>
                      <td>{$row['version']}</td>
                  </tr>";
        }
        echo "<tr><td colspan='7'>Total price: $total_price</td></tr>";
    } else {
        echo "<tr><td colspan='7'>Not Found!</td></tr>";
    }
    ?>
</table>

</body>
</html>